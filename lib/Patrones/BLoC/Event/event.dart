class HomesEvent {}

class LoadedEvent extends HomesEvent {}

class NotLoadedEvent extends HomesEvent {
  final int position;

  NotLoadedEvent(this.position);

  //
}
